using IdentityServer4.Models;

namespace identityapi.Models
{
    public class ErrorViewModel
    {
        public ErrorMessage Error { get; set; }
    }
}