using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.IO;
using Microsoft.AspNetCore;
using Serilog;
using IdentityServer4.EntityFramework.DbContexts;
using identityapi.Data;
using identityapi.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using identityapi.POCO;

namespace identityapi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //CreateHostBuilder(args).Build().Run();
            try
            {
                var configuration = GetConfiguration();
                var host = BuildWebHost(configuration, args);

                //host.MigrateDbContext<PersistedGrantDbContext>((_, __) => { })
                //    .MigrateDbContext<ApplicationDbContext>((context, services) =>
                //    {
                //        var env = services.GetService<IWebHostEnvironment>();
                //        var logger = services.GetService<ILogger<ApplicationDbContextSeed>>();
                //        var settings = services.GetService<IOptions<AppSettings>>();

                //        new ApplicationDbContextSeed()
                //            .SeedAsync(context, env, logger, settings)
                //            .Wait();
                //    })
                //    .MigrateDbContext<ConfigurationDbContext>((context, services) =>
                //    {
                //        new ConfigurationDbContextSeed()
                //            .SeedAsync(context, configuration)
                //            .Wait();
                //    });

                host.Run();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });


        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private static IWebHost BuildWebHost(IConfiguration configuration, string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .CaptureStartupErrors(false)
                .ConfigureAppConfiguration(x => x.AddConfiguration(configuration))
                .UseStartup<Startup>()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseSerilog()
                .Build();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static IConfiguration GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            var config = builder.Build();

            if (config.GetValue<bool>("UseVault", false))
            {
                builder.AddAzureKeyVault(
                    $"https://{config["Vault:Name"]}.vault.azure.net/",
                    config["Vault:ClientId"],
                    config["Vault:ClientSecret"]);
            }

            return builder.Build();
        }
    }
}
