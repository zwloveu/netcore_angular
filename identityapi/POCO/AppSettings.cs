﻿using System;

namespace identityapi.POCO
{
    public class AppSettings
    {
        public string MvcClient { get; set; }
        public bool UseCustomizationData { get; set; }
    }
}