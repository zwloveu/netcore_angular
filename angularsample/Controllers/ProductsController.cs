using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace NetCoreAngularSample.Controllers
{
    public class ProductCollection: List<Product>{}

    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
    
    [Route("api")]
    public class ProductsController : ControllerBase
    {
         private readonly IOptionsSnapshot<ProductCollection> _products;

        public ProductsController(IOptionsSnapshot<ProductCollection> products)
        {
            _products = products;
        }
        [Route("products")]
        public async Task<IActionResult> Index()
        {            
            await Task.CompletedTask;
            return Ok(_products.Value);
        }
    }
}