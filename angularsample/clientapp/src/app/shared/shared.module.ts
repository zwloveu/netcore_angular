import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

// components
import { TopBarComponent } from './components/top-bar/top-bar.component';


// services
import { CartService } from './services/cart.service';

@NgModule({
  declarations: [    
    TopBarComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
  ],
  exports: [
    // modules
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,

    // components
    TopBarComponent
  ],
  providers: [
    CartService
  ]
})
export class SharedModule { }
