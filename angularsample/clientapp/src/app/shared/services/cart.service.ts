import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ShippingCost } from '../../types/shipping-cost';
import { Product } from '../../types/product';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  items: Product[] = [];

  constructor(
    private httpClient: HttpClient
  ) { }

  addToCart(product: Product): void {
    this.items.push(product);
  }

  getItems(): Product[] {
    return this.items;
  }

  clearCart() {
    this.items = [];
    return this.getItems();
  }

  getShippingCosts(): Observable<ShippingCost[]> {
    return this.httpClient.get<ShippingCost[]>("../../../assets/shipping.json");
  }
}
