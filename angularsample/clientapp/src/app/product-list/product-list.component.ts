import { Component, OnInit } from '@angular/core';
import { products } from '../data/products';
import { Product } from '../types/product';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  
  private _products: Product[] = products;

  constructor() { }

  ngOnInit(): void {
  }

  share(product: Product) {
    window.alert(`The product has been shared! ProductId : ${product.productId}`);
  }

  onNotify(product: Product) {
    window.alert(`You will be notified when the product ${product.productId} goes on`)
  }

  get products(): Product[] {
    return this._products;
  }

}
