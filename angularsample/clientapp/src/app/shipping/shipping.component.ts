import { Component, OnInit } from '@angular/core';
import { CartService } from '../shared/services/cart.service';
import { ShippingCost } from '../types/shipping-cost';

@Component({
  selector: 'app-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['./shipping.component.scss']
})
export class ShippingComponent implements OnInit {

  shippingCosts: ShippingCost[] = [];

  constructor(
    private cartService: CartService
  ) { }

  ngOnInit(): void {
    this.cartService.getShippingCosts()
      .subscribe(o => this.shippingCosts =o);
  }

}
