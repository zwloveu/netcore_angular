import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { products } from '../data/products';
import { CartService } from '../shared/services/cart.service';
import { Product } from '../types/product';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
})
export class ProductDetailsComponent implements OnInit {
  
  product?: Product = <Product>{};

  constructor(
    private route: ActivatedRoute,
    private cartService: CartService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      let productId = params.get('productId');
      if (productId) {
        this.product = products.find((o) => o.productId == +productId!);
      }
    });
  }

  addToCart() {
    this.cartService.addToCart(this.product!);
    window.alert(`your product ${this.product!.productId} has been added to cart`);
  }
}
