import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CartService } from '../shared/services/cart.service';
import { CartItem } from '../types/cart-item';
import { Product } from '../types/product';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  items: Product[] = [];

  checkoutForm: FormGroup;

  constructor(
    private cartService: CartService,
    private formBuilder: FormBuilder
  ) { 
    this.checkoutForm = this.formBuilder.group({
      name: "",
      address: ""
    });
  }

  ngOnInit(): void {
    this.items = this.cartService.getItems();
  }

  submit(customerData: CartItem) {
    this.items = this.cartService.clearCart();
    this.checkoutForm.reset();

    // console.warn("your order has been submitted", customerData)
  }

}
