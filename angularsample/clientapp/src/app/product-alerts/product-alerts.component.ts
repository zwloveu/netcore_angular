import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Product } from '../types/product';

@Component({
  selector: 'app-product-alerts',
  templateUrl: './product-alerts.component.html',
  styleUrls: ['./product-alerts.component.scss']
})
export class ProductAlertsComponent implements OnInit {
  
  @Input() 
  product: Product = <Product>{};;

  @Output() 
  notify = new EventEmitter<Product>();

  constructor() { }

  ngOnInit(): void {
  }

  raiseNotify(): void {
    this.notify.emit(this.product);
  }

}
