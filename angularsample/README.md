# NetCoreWithAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

# Build With DotNetCore

## Create dotnet web under this angular app
```
dotnet new web
```

## Specify output dest in angular.json
- `wwwroot` is the default static resources folder in dotnetcore
```
"outputPath": "wwwroot"
```

## Add code in `Startup` Configure method
```
app.UseDefaultFiles();
app.UseStaticFiles();
```

## Update the `scripts` section in `package.json`
```
"build": "ng build && dotnet build && dotnet watch run"
```

# Docker Image/Container
```
docker build -t netcoreangular:1.0.0 .
docker run -it --name netcoreangular_10080_1.0.0 -p 10080:80 -d netcoreangular:1.0.0
```

# Add Node/Docker support
1. Add node deploy packages
```
npm install express -D
npm install json-server -D
npm install connect-history-api-fallback -D
```
2. Update /app/data/remoteDataSource.ts, use local url for web service
```
products: "/api/products",
orders: "/api/orders"
```
3. Add `server.js` to create a webserver using express and json-server
4. run `node server.js` to run the application
5. add docker file `Dockerfile.NodeExpress.AfterPublish`
