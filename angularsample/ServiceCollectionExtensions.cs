using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace NetCoreAngularSample
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection ConfigConfiguration<T>(
            this IServiceCollection services,
            string configPath, string configFileName, string env, string section = null)
            where T : class
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(configPath)
                .AddJsonFile($"{configFileName}.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"{configFileName}.{env}.json", optional: true)
                .AddEnvironmentVariables()
                .Build();

            return services.Configure<T>(section is null ? configuration : configuration.GetSection(section));
        }
    }
}