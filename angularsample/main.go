// main package to run the static site using GO
package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

// JSONConfig represents the data for data.json
type JSONConfig struct {
	Products []Product `json:"products"`
}

// Product represents the section "products" in data.json
type Product struct {
	ID          int     `json:"id"`
	Name        string  `json:"name"`
	Category    string  `json:"category"`
	Description string  `json:"description"`
	Price       float64 `json:"Price"`
}

var js JSONConfig = JSONConfig{}

func init() {
	data, err := ioutil.ReadFile("./data.json")
	if err != nil {
		panic("json data load failed")
	}

	err = json.Unmarshal(data, &js)
	if err != nil {
		panic("json data load failed")
	}
}

func main() {
	// mux := http.NewServeMux()

	// mux.Handle("/html/", http.StripPrefix("/html", http.FileServer(http.Dir("./wwwroot"))))
	// mux.Handle("/js/", http.StripPrefix("/js", http.FileServer(http.Dir("./wwwroot"))))
	// mux.Handle("/css/", http.StripPrefix("/css", http.FileServer(http.Dir("./wwwroot"))))

	http.HandleFunc("/api/products", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("content-type", "application/json")
		msg, err := json.Marshal(js.Products)
		if err != nil {
			log.Fatal(err)
		}
		w.Write(msg)
	})

	// mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
	// 	http.ServeFile(w, r, "./wwwroot/index.html")
	// })

	http.Handle("/", http.FileServer(http.Dir("clientapp/dist/clientapp")))

	server := &http.Server{
		Addr: "localhost:3000",
		//Handler: mux,
	}

	log.Println("Listening on :3000...")
	server.ListenAndServe()
}
