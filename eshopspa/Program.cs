using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace EShopSPA
{
    public class AppSettings
    {
        public string IdentityUrl { get; set; }
        public string BasketUrl { get; set; }
        public string MarketingUrl { get; set; }

        public string PurchaseUrl { get; set; }
        public string SignalrHubUrl { get; set; }

        public string ActivateCampaignDetailFunction { get; set; }
        public bool UseCustomizationData { get; set; }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>().UseContentRoot(Directory.GetCurrentDirectory())
                    .ConfigureAppConfiguration((builderContext, config) =>
                    {
                        config.AddEnvironmentVariables();
                    })
                    .ConfigureLogging((hostingContext, builder) =>
                    {
                        builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                        builder.AddConsole();
                        builder.AddDebug();
                    })
                    .UseSerilog((builderContext, config) =>
                    {
                        config
                            .MinimumLevel.Information()
                            .Enrich.FromLogContext()
                            .WriteTo.Console()
                            .WriteTo.File(builderContext.HostingEnvironment.ContentRootPath + "/logs/stdout/log",
                                rollingInterval: RollingInterval.Day
                            );
                    });
                });
    }
}
