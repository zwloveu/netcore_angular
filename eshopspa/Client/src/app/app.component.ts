import { Title } from '@angular/platform-browser';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Subscription } from 'rxjs';

//import { SecurityService } from './shared/services/security.service';
import { ConfigurationService } from './shared/services/configuration.service';
//import { SignalrService } from './shared/services/signalr.service';
//import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  Authenticated: boolean = true;
  //subscription: Subscription;

  constructor(private titleService: Title,
    //private securityService: SecurityService,
    private configurationService: ConfigurationService,
    //private signalrService: SignalrService,
    //private toastr: ToastrService,
    //vcr: ViewContainerRef
  ) {
    // TODO: Set Taster Root (Overlay) container
    //this.toastr.setRootViewContainerRef(vcr);
    //this.Authenticated = this.securityService.IsAuthorized;
    this.Authenticated = true;
  }

  ngOnInit() {
    console.log('app on init');
    //this.subscription = this.securityService.authenticationChallenge$.subscribe(res => this.Authenticated = res);

    //Get configuration from server environment variables:
    console.log('configuration');
    this.configurationService.load();

    this.setTitle('EShopNetCore');
  }

  private setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }
}
