import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasketComponent } from './basket.component';
import { BasketStatusComponent } from './basket-status/basket-status.component';
import { BasketService } from './basket.service';


@NgModule({
  declarations: [BasketComponent, BasketStatusComponent],
  imports: [
    CommonModule
  ],
  providers: [
    BasketService
  ],
  exports:[
    BasketStatusComponent
  ]
})
export class BasketModule {}