import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampaignsDetailComponent } from './campaigns-detail/campaigns-detail.component';
import { CampaignsComponent } from './campaigns.component';
import { CampaignsService } from './campaigns.service';


@NgModule({
  declarations: [CampaignsDetailComponent, CampaignsComponent],
  imports: [
    CommonModule
  ],
  providers: [
    CampaignsService
  ],
  exports: [

  ]
})
export class CampaignsModule { }
