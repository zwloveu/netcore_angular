import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogService } from './catalog.service';
import { CatalogComponent } from './catalog.component';

@NgModule({
  declarations: [CatalogComponent],
  imports: [
    CommonModule
  ],
  providers: [
    CatalogService
  ],
  exports: [

  ]
})
export class CatalogModule { }
