import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersComponent } from './orders.component';
import { OrdersService } from './orders.service';
import { OrdersNewComponent } from './orders-new/orders-new.component';
import { OrdersDetailComponent } from './orders-detail/orders-detail.component';


@NgModule({
  declarations: [OrdersComponent, OrdersNewComponent, OrdersDetailComponent],
  imports: [
    CommonModule
  ],
  providers: [
    OrdersService
  ],
  exports: [

  ]
})
export class OrdersModule { }