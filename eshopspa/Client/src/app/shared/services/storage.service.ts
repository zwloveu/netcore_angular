import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private storage: any;

  constructor() { 
    this.storage = localStorage;
  }

  public retrieve<T>(key: string): T | undefined {
    let item = this.storage.getItem(key);

    if(item && item !== 'undefined') {
      return JSON.parse(item) as T;
    }

    return undefined;
  }

  public store(key: string, value: any): void {
    this.storage.setItem(key,JSON.stringify(value));
  }
}
