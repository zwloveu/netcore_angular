import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Services:
import { StorageService } from './services/storage.service';

// Components:
import { HeaderComponent } from './components/header/header.component';
import { PagerComponent } from './components/pager/pager.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

// Pipes:
import { MyUppercasePipe } from './pipes/my-uppercase.pipe';

@NgModule({
  declarations: [
    HeaderComponent, 
    PagerComponent, 
    PageNotFoundComponent, 
    MyUppercasePipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    // No need to export as these modules don't expose any components/directive etc'
    HttpClientModule,
    HttpClientJsonpModule
  ],
  exports: [
    // Modules
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    // Providers, Components, directive, pipes
    HeaderComponent,
    PagerComponent,
    PageNotFoundComponent,
    MyUppercasePipe
  ],
  providers:[
    StorageService
  ]
})
export class SharedModule { }
