using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace EShopSPA.Hubs
{
    public class NotificationHub : Hub
    {
        public async Task NewMessage(long username, string message)
        {
            await Clients.All.SendAsync("messageReceived", username, message);
        }
    }
}